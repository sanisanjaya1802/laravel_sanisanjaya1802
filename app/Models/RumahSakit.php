<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RumahSakit extends Model
{
    use HasFactory;
    protected $table = 'rumah_sakit'; // Sesuaikan nama tabel dengan yang sebenarnya di database

    protected $fillable = [
        'nama', 'alamat', 'email', 'telepon',
    ];
}
