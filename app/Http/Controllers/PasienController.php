<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pasien;
use App\Models\RumahSakit;

class PasienController extends Controller
{
    public function index()
    {
        $pasien = Pasien::with('rumahSakit')->get();
        return view('pasien.index', compact('pasien'));
    }

    public function create()
    {
        // Ambil semua data rumah sakit untuk ditampilkan dalam dropdown
        $rumahSakit = RumahSakit::all();

        // Tampilkan formulir untuk membuat data pasien baru
        return view('pasien.create', compact('rumahSakit'));
    }

    public function store(Request $request)
    {
        // Validasi data yang dikirimkan melalui formulir
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required',
            'id_rumah_sakit' => 'required|exists:rumah_sakit,id',
        ]);

        // Simpan data pasien ke dalam database
        Pasien::create($request->all());

        // Redirect ke halaman index dengan pesan sukses
        return redirect()->route('pasien.index')->with('success', 'Data Pasien berhasil disimpan.');
    }

    public function edit($id)
    {
        // Temukan data pasien berdasarkan ID
        $pasien = Pasien::findOrFail($id);
        // Ambil semua data rumah sakit untuk ditampilkan dalam dropdown
        $rumahSakit = RumahSakit::all();
        $selectedIdRs = $pasien->id_rumah_sakit;

        // Tampilkan formulir untuk mengedit data pasien
        return view('pasien.edit', ['pasien' => $pasien, 'rumahSakit' => $rumahSakit, 'selectedIdRs' => $selectedIdRs]);
    }

    public function update(Request $request, $id)
    {
        // Validasi data yang dikirimkan melalui formulir
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'no_telepon' => 'required',
            'id_rumah_sakit' => 'required|exists:rumah_sakit,id',
        ]);

        // Temukan data pasien berdasarkan ID dan perbarui dengan data baru
        $pasien = Pasien::findOrFail($id);
        $pasien->update($request->all());

        // Redirect ke halaman index dengan pesan sukses
        return redirect()->route('pasien.index')->with('success', 'Data Pasien berhasil diperbarui.');
    }

    public function destroy($id)
    {
        // Temukan data pasien berdasarkan ID dan hapus
        $pasien = Pasien::findOrFail($id);
        $pasien->delete();

        // Redirect ke halaman index dengan pesan sukses
        return response()->json(['message' => 'Rumah Sakit berhasil dihapus.']);
    }
    public function getRumahSakit()
    {
        $rumahSakit = RumahSakit::all();
        return response()->json($rumahSakit);
    }
    public function getPasienByRumahSakit($rumahSakitId)
    {


        if ($rumahSakitId === "0") {
            $pasien = Pasien::with('rumahSakit')->get();
        } else {
            // Jika rumah sakit dipilih, ambil data pasien berdasarkan rumah sakit yang dipilih
            $pasien = Pasien::with('rumahSakit')->where('id_rumah_sakit', $rumahSakitId)->get();
        }
        // Mengembalikan data pasien dalam format JSON
        return response()->json($pasien);
    }
}
