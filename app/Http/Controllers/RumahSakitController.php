<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RumahSakit;

class RumahSakitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rumahSakit = RumahSakit::all();
        return view('rumah_sakit.index', compact('rumahSakit'));
    }

    public function create()
    {
        return view('rumah_sakit.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required|email|unique:rumah_sakit',
            'telepon' => 'required',
        ]);

        RumahSakit::create($request->all());

        return redirect()->route('rumah_sakit.index')->with('success', 'Rumah Sakit berhasil ditambahkan.');
    }
    public function edit($id)
    {
        $rumahSakit = RumahSakit::findOrFail($id);

        return view('rumah_sakit.edit', compact('rumahSakit'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'email' => 'required|email|unique:rumah_sakit,email,' . $id,
            'telepon' => 'required',
        ]);
        $rumahSakit = RumahSakit::findOrFail($id);
        $rumahSakit->update($request->all());

        return redirect()->route('rumah_sakit.index')->with('success', 'Rumah Sakit berhasil diperbarui.');
    }

    public function destroy($id)
    {
        $rumahSakit = RumahSakit::findOrFail($id);

        // Menghapus data rumah sakit

        $rumahSakit->delete();
        return response()->json(['message' => 'Rumah Sakit berhasil dihapus.']);
        // return redirect()->route('rumah_sakit.index')->with('success', 'Rumah Sakit berhasil dihapus.');
    }
}
