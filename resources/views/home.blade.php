@extends('layouts.app')

@section('content')
<!-- resources/views/dashboard.blade.php -->
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <ul>
                        <li><a href="{{ route('rumah_sakit.index') }}">Data Rumah Sakit</a></li>
                        <li><a href="{{ route('pasien.index') }}">Data Pasien</a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection