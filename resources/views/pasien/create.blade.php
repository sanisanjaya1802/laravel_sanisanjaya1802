<!-- resources/views/pasien/create.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Data Pasien</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('pasien.store') }}">
                        @csrf

                        <div class="form-group">
                            <label for="nama">Nama:</label>
                            <input type="text" name="nama" id="nama" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="alamat">Alamat:</label>
                            <input type="text" name="alamat" id="alamat" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="telepon">Telepon:</label>
                            <input type="text" name="no_telepon" id="no_telepon" class="form-control" required>
                        </div>

                        <div class="form-group">
                            <label for="rumah_sakit_id">Rumah Sakit:</label>
                            <select name="id_rumah_sakit" id="id_rumah_sakit" class="form-control" required>
                                <option value="">Pilih Rumah Sakit</option>
                                @foreach($rumahSakit as $rs)
                                <option value="{{ $rs->id }}">{{ $rs->nama }}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection