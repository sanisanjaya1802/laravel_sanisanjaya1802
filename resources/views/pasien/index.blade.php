<!-- resources/views/pasien/index.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Daftar Data Pasien</div>

                <div class="card-body">
                    <a href="{{ route('pasien.create') }}" class="btn btn-primary mb-3">Tambah Pasien</a>
                    <div class="form-group">
                        <label for="rumah_sakit_filter">Filter berdasarkan Rumah Sakit:</label>
                        <select id="rumah_sakit_filter" class="form-control">
                            <option value="">Semua Rumah Sakit</option>
                            <!-- Opsi akan diisi melalui permintaan Ajax -->
                        </select>
                    </div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Alamat</th>
                                <th scope="col">Telepon</th>
                                <th scope="col">Rumah Sakit</th>
                                <th scope="col">Aksi</th>
                            </tr>
                        </thead>
                        <tbody id="pasien_table_body">
                            @foreach($pasien as $key => $p)
                            <tr>
                                <th scope="row">{{ $key + 1 }}</th>
                                <td>{{ $p->nama }}</td>
                                <td>{{ $p->alamat }}</td>
                                <td>{{ $p->no_telepon }}</td>
                                <td>{{ $p->rumahSakit->nama }}</td>
                                <td>
                                    <a href="{{ route('pasien.edit', $p->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                    <button class="btn btn-danger btn-delete" data-id="{{ $p->id }}" onclick="return confirm('Apakah Anda yakin ingin menghapus?')">Hapus</button>

                                    <!-- <form action="{{ route('pasien.destroy', $p->id) }}" method="POST" style="display: inline-block;">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Hapus</button>
                                    </form> -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        $('.btn-delete').click(function() {
            var pasienId = $(this).data('id');

            $.ajax({
                url: '/pasien/' + pasienId,
                type: 'delete',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    alert(response.message);
                    // Reload halaman atau hapus baris dari tabel
                    location.reload();
                },
                error: function(xhr) {
                    alert('Terjadi kesalahan: ' + xhr.responseText);
                }
            });
        });
    });

    $(document).ready(function() {
        // Panggil fungsi untuk mengisi dropdown filter dengan daftar rumah sakit saat halaman dimuat
        populateRumahSakitDropdown();
        // Tangani perubahan pada dropdown filter
        $('#rumah_sakit_filter').change(function() {
            var rumahSakitId = $(this).val();
            getPasienByRumahSakit(rumahSakitId);
        });
    });

    function populateRumahSakitDropdown() {
        $.get('/get-rumah-sakit', function(response) {
            var options = '<option value="0">Semua Rumah Sakit</option>';
            $.each(response, function(index, rumahSakit) {
                options += '<option value="' + rumahSakit.id + '">' + rumahSakit.nama + '</option>';
            });
            $('#rumah_sakit_filter').html(options);
        });
    }

    function getPasienByRumahSakit(rumahSakitId) {
        $.get('/get-pasien-by-rumah-sakit/' + rumahSakitId, function(response) {
            // Tampilkan data pasien yang diperoleh dari respons Ajax
            updatePasienTable(response);
        });
    }
    // Fungsi untuk memperbarui tabel dengan data pasien yang diperoleh dari respons Ajax
    function updatePasienTable(pasienData) {
        var tableBody = $('#pasien_table_body');
        tableBody.empty(); // Menghapus semua baris yang ada dalam tabel

        // Membuat baris baru untuk setiap data pasien dan menambahkannya ke dalam tabel
        $.each(pasienData, function(index, pasien) {
            var newRow = '<tr>' +
                '<th scope="row">' + (index + 1) + '</th>' +
                '<td>' + pasien.nama + '</td>' +
                '<td>' + pasien.alamat + '</td>' +
                '<td>' + pasien.no_telepon + '</td>' +
                '<td>' + pasien.rumah_sakit.nama + '</td>' +
                '<td>' +
                '<a href="/pasien/' + pasien.id + '/edit" class="btn btn-sm btn-primary">Edit</a>' +
                '<button class="btn btn-danger btn-delete" data-id="' + pasien.id + '" onclick="return confirm(\'Apakah Anda yakin ingin menghapus?\')">Hapus</button>' +
                '</td>' +
                '</tr>';
            tableBody.append(newRow);
        });
    }
</script>
@endsection