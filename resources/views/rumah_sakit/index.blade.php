<!-- resources/views/rumah_sakit/index.blade.php -->

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Data Rumah Sakit</div>
                <div class="card-body">
                    <a href="{{ route('rumah_sakit.create') }}" class="btn btn-primary mb-3">Tambah Rumah Sakit</a>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Rumah Sakit</th>
                                <th>Alamat</th>
                                <th>Email</th>
                                <th>Telepon</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rumahSakit as $rs)
                            <tr>
                                <td>{{ $rs->id }}</td>
                                <td>{{ $rs->nama }}</td>
                                <td>{{ $rs->alamat }}</td>
                                <td>{{ $rs->email }}</td>
                                <td>{{ $rs->telepon }}</td>
                                <td>
                                    <a href="{{ route('rumah_sakit.edit', $rs->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                    <button class="btn btn-danger btn-delete" data-id="{{ $rs->id }}" onclick="return confirm('Apakah Anda yakin ingin menghapus?')">Hapus</button>

                                    <!-- <form action="{{ route('rumah_sakit.destroy', $rs->id) }}" method="POST" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus?')">Hapus</button>
                                    </form> -->
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    $(document).ready(function() {
        $('.btn-delete').click(function() {
            var rumahSakitId = $(this).data('id');

            $.ajax({
                url: '/rumah_sakit/' + rumahSakitId,
                type: 'delete',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response) {
                    alert(response.message);
                    // Reload halaman atau hapus baris dari tabel
                    location.reload();
                },
                error: function(xhr) {
                    alert('Terjadi kesalahan: ' + xhr.responseText);
                }
            });
        });
    });
</script>
@endsection