<?php

use App\Http\Controllers\PasienController;
use App\Http\Controllers\RumahSakitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();
// routes/web.php

// Route::resource('rumah_sakit', 'RumahSakitController');
Route::get('rumah_sakit', [RumahSakitController::class, 'index'])->name('rumah_sakit.index');
Route::get('rumah_sakit/create', [RumahSakitController::class, 'create'])->name('rumah_sakit.create');
Route::post('rumah_sakit/store', [RumahSakitController::class, 'store'])->name('rumah_sakit.store');
Route::delete('rumah_sakit/{id}', [RumahSakitController::class, 'destroy'])->name('rumah_sakit.destroy');
Route::get('rumah_sakit/{id}/edit', [RumahSakitController::class, 'edit'])->name('rumah_sakit.edit');
Route::put('/rumah_sakit/{id}', [RumahSakitController::class, 'update'])->name('rumah_sakit.update');

Route::get('pasien', [PasienController::class, 'index'])->name('pasien.index');
Route::get('pasien/create', [PasienController::class, 'create'])->name('pasien.create');
Route::post('pasien/store', [PasienController::class, 'store'])->name('pasien.store');
Route::delete('pasien/{id}', [PasienController::class, 'destroy'])->name('pasien.destroy');
Route::get('pasien/{id}/edit', [PasienController::class, 'edit'])->name('pasien.edit');
Route::put('/pasien/{id}', [PasienController::class, 'update'])->name('pasien.update');
Route::get('/get-rumah-sakit', [PasienController::class, 'getRumahSakit'])->name('pasien.getRumahSakit');
Route::get('/get-pasien-by-rumah-sakit/{id}', [PasienController::class, 'getPasienByRumahSakit'])->name('pasien.getPasienByRumahSakit');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
