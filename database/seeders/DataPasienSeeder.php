<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pasien;

class DataPasienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pasien::create([
            'nama' => 'John Doe',
            'alamat' => 'Jalan ABC No. 123',
            'no_telepon' => '123456789',
            'id_rumah_sakit' => 1 // Ganti dengan ID rumah sakit yang valid
        ]);

        Pasien::create([
            'nama' => 'Jane Smith',
            'alamat' => 'Jalan XYZ No. 456',
            'no_telepon' => '987654321',
            'id_rumah_sakit' => 2 // Ganti dengan ID rumah sakit yang valid
        ]);

        // Tambahkan data lain sesuai kebutuhan Anda
    }
}
