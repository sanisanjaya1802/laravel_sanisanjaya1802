<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RumahSakit;

class RumahSakitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RumahSakit::create([
            'nama' => 'Rumah Sakit A',
            'alamat' => 'Jalan A No. 123',
            'email' => 'rs_a@example.com',
            'telepon' => '123456789'
        ]);

        RumahSakit::create([
            'nama' => 'Rumah Sakit B',
            'alamat' => 'Jalan B No. 456',
            'email' => 'rs_b@example.com',
            'telepon' => '987654321'
        ]);

    }
}
